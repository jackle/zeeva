from osv import osv, fields
import openerp.addons.decimal_precision as dp
from openerp import netsvc


class sale_order(osv.osv):
    _inherit = 'sale.order'

    def default_get(self, cr, uid, fields, context=None):
        if context is None: context = {}
        res = super(sale_order, self).default_get(cr, uid, fields, context=context)
        employee_obj = self.pool.get('hr.employee')
        employee_ids = employee_obj.search(cr, uid, [('user_id', '=', uid)])
        if employee_ids:
            emp = employee_obj.browse(cr, uid, employee_ids)[0]
            if emp.department_id and emp.department_id.name == 'Sales Department':
                res['user_id'] = uid

        employee_ids = employee_obj.search(cr, uid, [('user_id', '=', uid)])
        if employee_ids:
            emp = employee_obj.browse(cr, uid, employee_ids)[0]
            if emp.department_id and emp.department_id.name == 'Merchandising Department':
                res['merchandiser'] = uid
        return res

    def _balance_qty(self, cr, uid, ids, prop, arg, context=None):
        res = {}
        move_obj = self.pool.get('stock.move')
        for sale_order in self.browse(cr, uid, ids, context=context):
            balance_qty = 0.0
            qty = 0.0
            qty_done = 0.0
            lst_move = move_obj.search(cr, uid, [('sale_line_id.order_id', '=', sale_order.id),
                                                 ('state', 'in', ['done'])])
            qty = sum(line.product_uom_qty for line in sale_order.order_line)
            balance_qty = qty - sum(line.product_qty for line in move_obj.browse(cr, uid, lst_move))
            res[sale_order.id] = balance_qty
        return res

    _columns = {
        'balance_qty': fields.function(_balance_qty, string='Balanced Qty',
                                       digits_compute=dp.get_precision('Account')),

    }


    def action_ship_create(self, cr, uid, ids, context=None):
        for order in self.browse(cr, uid, ids, context=context):
            self._create_procurements(cr, uid, order, order.order_line, None, context=context)
        return True

    def _create_procurements(self, cr, uid, order, order_lines, picking_id=False, context=None):
        procurement_obj = self.pool.get('procurement.order')
        proc_ids = []

        for line in order_lines:
            if line.state == 'done':
                continue

            date_planned = self._get_date_planned(cr, uid, order, line, order.date_order, context=context)

            if line.product_id:
                if line.product_id.type in ('product', 'consu'):
                    move_id = False

                proc_id = procurement_obj.create(cr, uid,
                                                 self._prepare_order_line_procurement(cr, uid, order, line, move_id,
                                                                                      date_planned, context=context))
                proc_ids.append(proc_id)
                line.write({'procurement_id': proc_id})
                self.ship_recreate(cr, uid, order, line, move_id, proc_id)

        wf_service = netsvc.LocalService("workflow")
        for proc_id in proc_ids:
            wf_service.trg_validate(uid, 'procurement.order', proc_id, 'button_confirm', cr)

        val = {}
        if order.state == 'shipping_except':
            val['state'] = 'progress'
            val['shipped'] = False

            if (order.order_policy == 'manual'):
                for line in order.order_line:
                    if (not line.invoiced) and (line.state not in ('cancel', 'draft')):
                        val['state'] = 'manual'
                        break
        order.write(val)
        return True

    def action_button_confirm(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        zwip_obj = self.pool.get('zwip.zwip')

        res = super(sale_order, self).action_button_confirm(cr, uid, ids, context=None)
        # After confirm SO, create zwip with each SO line
        for so in self.browse(cr, uid, ids, context=context):
            for line in so.order_line:
                if not line.product_id or line.product_id.type == 'service': continue

                vals = {
                    'sale_line_id': line.id,
                }
                zwip_obj.create(cr, uid, vals, context=context)

        return res

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        invoice_vals = super(sale_order, self)._prepare_invoice(cr, uid, order, lines, context=context)
        invoice_vals.update({
            'etd': order.eta or '',
            'port_of_loading': order.port_of_loading and order.port_of_loading.id or False,
            'port_of_discharge': order.port_of_discharge and order.port_of_discharge.id or False,
            'incoterm': order.incoterm and order.incoterm.id or False,
        })
        return invoice_vals

    def action_cancel(self, cr, uid, ids, context=None):
        zwip_obj = self.pool.get('zwip.zwip')
        sale_line_ids = []
        for so in self.browse(cr, uid, ids, context=context):
            for line in so.order_line:
                sale_line_ids += [line.id]
        zwip_ids = zwip_obj.search(cr, uid, [('sale_line_id', 'in', sale_line_ids)])
        zwip_obj.unlink(cr, uid, zwip_ids, context)
        return super(sale_order, self).action_cancel(cr, uid, ids, context=context)
