from datetime import date
from openerp.tools.translate import _
from osv import osv, fields
from openerp import SUPERUSER_ID
import openerp.addons.decimal_precision as dp


class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'

    def _balance_line_qty(self, cr, uid, ids, prop, arg, context=None):
        res = {}
        move_obj = self.pool.get('stock.move')
        for line in self.browse(cr, uid, ids, context=context):
            balance_qty = line.product_uom_qty
            res.setdefault(line.id, line.product_uom_qty)
            lst_move = move_obj.search(cr, uid, [('sale_line_id', '=', line.id),
                                                 ('state', 'in', ['done'])])
            if lst_move:
                balance_qty = line.product_uom_qty - sum(line.product_qty for line in move_obj.browse(cr, uid, lst_move))
            res[line.id] = balance_qty
        return res

    def _get_sm(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('stock.move').browse(cr, uid, ids, context=context):
            if line.sale_line_id:
                result[line.sale_line_id.id] = True
        return result.keys()

    _columns = {
        'balance_line_qty': fields.function(_balance_line_qty, string='Balanced Qty',type="float",
                                            store={
                                                'stock.move': (_get_sm, ['state'], 20),
                                                'sale.order.line': (lambda self, cr, uid, ids, context=None: ids, None, 10),
                                            },
                                            digits_compute=dp.get_precision('Account')),
    }

    def name_get(self, cr, uid, ids, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        if not ids:
            return []
        res = []
        lst_sol = self.pool.get('sale.order.line').browse(cr, uid, ids, context=context)
        for sol in lst_sol:
            name = '%s' % (sol.order_id.name)
            if sol.name:
                name = '%s - %s' % (name, sol.name)
            elif sol.product_id:
                name = '%s - %s' % (name, sol.product_id.name)

            res.append((sol.id, name))
        return res


sale_order_line()
