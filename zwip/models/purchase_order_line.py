from datetime import date
from openerp.tools.translate import _
from osv import osv, fields
from openerp import SUPERUSER_ID


class purchase_order_line(osv.osv):
    _inherit = 'purchase.order.line'

    def unlink(self, cr, uid, ids, context=None):
        zwip_obj = self.pool.get('zwip.zwip')
        zwip_ids = zwip_obj.search(cr, uid, [('purchase_line_id', 'in', ids)])
        zwip_obj.write(cr, uid, zwip_ids, {'purchase_line_id': False})
        return super(purchase_order_line, self).unlink(cr, uid, ids, context=context)

