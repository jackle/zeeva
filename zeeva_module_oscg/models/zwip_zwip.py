from osv import osv, fields

class zwip_zwip(osv.osv):
    _inherit = 'zwip.zwip'
    _order = 'name desc'
    _rec_name = 'name'

    _columns = {
        'name': fields.related('sale_line_id', 'order_id', 'name', type='char',
                               string='SC NO', readonly=True,
                               store={
                                   _inherit: (lambda self, cr, uid, ids, c: ids, ['sale_line_id'], 10),
                               }),

        'sc_date_confirmed': fields.related('sale_line_id', 'order_id', 'date_confirm', type='date',
                                            string='SC Confirmation Date', readonly=True,
                                            store={
                                                _inherit: (lambda self, cr, uid, ids, c: ids, ['sale_line_id'], 10),
                                            }),
    }

