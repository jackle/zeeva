from datetime import date
from openerp.tools.translate import _
from osv import osv, fields
from openerp import SUPERUSER_ID

ZWIP_STATES = [('open', 'OPEN'), ('done', 'DONE')]


class zwip_zwip(osv.osv):
    _name = 'zwip.zwip'
    _rec_name = 'move_id'



    def _get_qty_in_picking(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        move_obj = self.pool.get('stock.move')
        zwips = self.browse(cr, uid, ids, context=context)
        for zwip in zwips:
            qty_shiped = 0.0
            qty_to_be_packed = 0.0
            product_qty_in_picking = 0.0
            sc_value_zwip = 0.0
            sc_remained_value = 0.0
            res[zwip.id] = {
                'qty_shiped': qty_shiped,
                'qty_to_be_packed': qty_to_be_packed,
                'product_qty_in_picking': product_qty_in_picking,
                'sc_value_zwip': sc_value_zwip,
                'sc_remained_value': sc_remained_value,
                'outstanding_qty': zwip.sale_line_id and zwip.sc_qty or 0.0,
            }
            if zwip.sale_line_id:
                sc_qty = zwip.sale_line_id.product_uos_qty or 0.0
                price_unit = zwip.sale_line_id.price_unit or 0.0
                discount = zwip.sale_line_id.discount and zwip.sale_line_id.discount or 0.0
                lst_sm = move_obj.search(cr, uid, [('state', 'not in', ['draft', 'done', 'cancel']),
                                                   ('sale_line_id', '=', zwip.sale_line_id.id)])

                product_qty_in_picking = sum(line.product_qty for line in move_obj.browse(cr, uid, lst_sm))

                lst_sm_done = move_obj.search(cr, uid, [('state', '=', 'done'),
                                                        ('sale_line_id', '=', zwip.sale_line_id.id)])
                qty_shiped = sum(line.product_qty for line in move_obj.browse(cr, uid, lst_sm_done))

                qty_to_be_packed = sc_qty - product_qty_in_picking - qty_shiped
                res[zwip.id]['qty_shiped'] = qty_shiped
                res[zwip.id]['outstanding_qty'] = sc_qty - qty_shiped
                res[zwip.id]['qty_to_be_packed'] = qty_to_be_packed
                res[zwip.id]['product_qty_in_picking'] = product_qty_in_picking
                res[zwip.id]['sc_value_zwip'] = qty_shiped * price_unit * (1 - discount / 100)
                res[zwip.id]['sc_remained_value'] = (sc_qty - qty_shiped) * price_unit * (1 - discount / 100)

        return res

    def _get_port(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        zwips = self.browse(cr, uid, ids, context=context)
        for zwip in zwips:
            res[zwip.id] = {
                'port_of_loading': False,
                'port_of_discharge': False,
                'etd': False,
                'eta': False,
                'vessel_freight': False,
            }
            if zwip.sale_line_id and zwip.move_id and zwip.move_id.picking_id:
                move = zwip.move_id.picking_id
                res[zwip.id]['port_of_loading'] = move.port_of_loading and move.port_of_loading.id or False
                res[zwip.id]['port_of_discharge'] = move.port_of_discharge and move.port_of_discharge.id or False
                res[zwip.id]['etd'] = move.ship_date or False
                res[zwip.id]['eta'] = move.eta or False
                res[zwip.id]['vessel_freight'] = move.vessel_name

        return res

    def _get_invoice(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        zwips = self.browse(cr, uid, ids, context=context)
        invoice_obj = self.pool.get('account.invoice')
        for zwip in zwips:
            res[zwip.id] = {
                'invoice_number': False,
                'due_date': False,
            }

            if zwip.sale_line_id and zwip.move_id and zwip.move_id.picking_id:
                picking = zwip.move_id.picking_id
                lst_invoice = invoice_obj.search(cr, uid, [('picking_id', '=', picking.id), ('state', 'in', ['open'])])
                if lst_invoice:
                    invoice = invoice_obj.browse(cr, uid, lst_invoice[0], context=context)

                    res[zwip.id]['invoice_number'] = invoice.number or ''
                    res[zwip.id]['due_date'] = invoice.date_due or False

        return res

    def _get_po(self, cr, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        pol_obj = self.pool.get('purchase.order.line')
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = {  ## qqq
                'purchase_line_id': False,
            }
            so = line.sale_line_id and line.sale_line_id.order_id and line.sale_line_id.order_id.name or ''
            # so = line and line.sale_id and line.sale_id.name or ''
            if so:
                pol = pol_obj.search(cr, uid, [('order_id.origin', 'like', so),
                                               ('order_id.state', 'not in', ['draft', 'cancel']),
                                               ('product_id', '=', line.product_id.id)])
                if pol:
                    res[line.id]['purchase_line_id'] = pol[0]

        return res

    def _state(self, cr, uid, ids, name, arg, context=None):
        res = {}
        temp_state = 'open'
        move_obj = self.pool.get('stock.move')
        invoice_obj = self.pool.get('account.invoice')
        for zwip in self.browse(cr, uid, ids, context=context):
            # if zwip.sale_line_id and zwip.sale_line_id.invoice_lines:
            #     invoice_qty = 0.0
            #     for inv_line in zwip.sale_line_id.invoice_lines:
            #         invoice_qty += inv_line.quantity
            #     if invoice_qty >= zwip.sc_qty:
            #         temp_state = 'done'
            if zwip.sale_line_id:
                sc_qty = zwip.sale_line_id.product_uos_qty or 0.0
                lst_sm_done = move_obj.search(cr, uid, [('state', '=', 'done'),
                                                        ('sale_line_id', '=', zwip.sale_line_id.id)])
                qty_shiped = sum(line.product_qty for line in move_obj.browse(cr, uid, lst_sm_done))
                outstanding_qty = sc_qty - qty_shiped
                if outstanding_qty <= 0.0:
                    temp_state = 'done'

            res[zwip.id] = temp_state
        return res

    def _get_so_line_ids(self, cr, uid, ids, context=None):
        if ids:
            sql = """SELECT DISTINCT order_line_id
                          from sale_order_line_invoice_rel
                          where invoice_id in %s"""
            cr.execute(sql, (tuple(ids),))
            res = cr.dictfetchall()
            if res:
                res = [item['order_line_id'] for item in res]
                zwip_ids = self.pool.get('zwip.zwip').search(cr, uid, [('sale_line_id', 'in', res)], context=context)
                if zwip_ids:
                    res = zwip_ids
                else:
                    res = []
            else:
                res = []
        else:
            res = []
        return list(set(res))

    def _get_zwip_ids(self, cr, uid, ids, context=None):
        res = []
        if ids:
            for sol in self.browse(cr, uid, ids, context=context):
                zwip_ids = self.search(cr, uid, [('id', '=', sol.id)], context=context)
                if zwip_ids:
                    res += zwip_ids
        return list(set(res))

    def _get_stock_move(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        zwip_obj = self.pool.get('zwip.zwip')
        zwip_ids = zwip_obj.search(cr, uid, [('move_id', 'in', ids)])
        return zwip_ids

    _columns = {
        'move_id': fields.many2one('stock.move', 'Stock Move'),

        'sale_line_id': fields.many2one('sale.order.line', string='Sale Order Line', readonly=True),

        'sale_id': fields.related('sale_line_id', 'order_id', type='many2one', relation='sale.order',
                                  string='SC NO', readonly=True, store=False),
        'company_id': fields.related('sale_line_id', 'order_id', 'company_id', type='many2one',
                                     relation='res.company', string='Company', readonly=True, store=False, ),

        'cust_po_ref': fields.related('sale_line_id', 'order_id', 'client_order_ref', type='char',
                                      string='Customer PO Ref. No.', readonly=True, store=False),

        'partner_id': fields.related('sale_line_id', 'order_id', 'partner_id', type='many2one',
                                     relation='res.partner', string='Customer', readonly=True, domain=[('parent_id', '=', False), ('customer', '=', True)]),

        'payment_term': fields.related('sale_line_id', 'order_id', 'payment_term',
                                       type='many2one', relation='account.payment.term',
                                       string='SC Payment Term', readonly=True),

        'sc_value': fields.related('sale_line_id', 'price_subtotal',
                                   type='float', string='SC Original Value (USD)', readonly=True),

        'sc_value_zwip': fields.function(_get_qty_in_picking, type='float', string='SC Value (USD) in zwip',
                                         multi='_get_qty_in_picking', readonly=True),

        'sc_remained_value': fields.function(_get_qty_in_picking, type='float', string='SC Remained Value (USD)',
                                             multi='_get_qty_in_picking', readonly=True),

        'sc_qty': fields.related('sale_line_id', 'product_uos_qty',
                                 type='float', string='Quantity', readonly=True, store=False),

        'account_manager_id': fields.related('sale_line_id', 'order_id', 'user_id',
                                             type='many2one', relation='res.users',
                                             string='Account Manager', readonly=True,
                                             store={
                                                 _name: (lambda self, cr, uid, ids, c: ids, ['sale_line_id'], 10),
                                             }),

        'merchandiser_id': fields.related('sale_line_id', 'order_id', 'merchandiser',
                                          type='many2one', relation='res.users',
                                          string='Merchandiser', readonly=True,
                                          store={
                                              _name: (lambda self, cr, uid, ids, c: ids, ['sale_line_id'], 10),
                                          }),

        # 'order_type': fields.related('sale_id', 'order_type', type='selection',
        #                              selection=[('new', 'New'), ('repeat', 'Repeat')],
        #                              string='Order Type', readonly=True, store=True),
        'order_type': fields.related('sale_line_id', 'order_id', 'order_type', type='selection',
                                     selection=[('new', 'New'), ('repeat', 'Repeat')],
                                     string='Order Type', readonly=True),
        'sc_delivery_date': fields.related('sale_line_id', 'order_id', 'ready_date',
                                           type='date', string='SC Delivery Date', readonly=True, store=False),

        'product_id': fields.related('sale_line_id', 'product_id', type='many2one',
                                     relation='product.product', string='Product', readonly=True, store=False),

        'item_code': fields.related('sale_line_id', 'product_id', 'default_code', type='char',
                                    string='Item Code', readonly=True),

        'product_qty_in_picking': fields.function(_get_qty_in_picking, type='float', string='Quantity in Packing List',
                                                  multi='_get_qty_in_picking'),

        'qty_shiped': fields.function(_get_qty_in_picking, type='float', string='Quantity Shipped',
                                      multi='_get_qty_in_picking'),
        'qty_to_be_packed': fields.function(_get_qty_in_picking, type='float', string='Quantity to be Packed',
                                            multi='_get_qty_in_picking'),

        'purchase_line_id': fields.function(_get_po, type='many2one', relation='purchase.order.line', ondelete='set null',
                                            store={
                                                _name: (lambda self, cr, uid, ids, c: ids, ['sale_line_id'], 10),
                                            },
                                            string='Purchase Order Line', multi='_get_po'),

        'purchase_id': fields.related('purchase_line_id', 'order_id', type='many2one',
                                      store={
                                          'zwip.zwip': (
                                              lambda self, cr, uid, ids, c={}: ids, ['purchase_line_id'], 20),
                                      },
                                      relation='purchase.order', string='PO NO', readonly=True),

        'item_description': fields.related('purchase_line_id', 'name', type='char',
                                           store={
                                               'zwip.zwip': (
                                                   lambda self, cr, uid, ids, c={}: ids, ['purchase_line_id'], 20),
                                           },
                                           string='Item Description', readonly=True),

        'quantity': fields.related('purchase_line_id', 'product_qty', type='float', string='Quantity',
                                   store={
                                       'zwip.zwip': (
                                           lambda self, cr, uid, ids, c={}: ids, ['purchase_line_id'], 20),
                                   },
                                   readonly=True),

        'supplier': fields.related('purchase_line_id', 'order_id', 'partner_id', type='many2one',
                                    store={
                                        'zwip.zwip': (
                                            lambda self, cr, uid, ids, c={}: ids, ['purchase_line_id'], 20),
                                    },
                                   relation='res.partner', string='Supplier', readonly=True, domain=[('parent_id', '=', False), ('supplier', '=', True)]),

        'supplier_payment_term': fields.related('purchase_line_id', 'order_id', 'payment_term_id',
                                                type='many2one', relation='account.payment.term',
                                                string='Supplier Payment Term', readonly=True),

        'po_delivery_date': fields.related('purchase_line_id', 'order_id', 'ready_date',
                                           type='date', string='Delivery Date',
                                           readonly=True),

        'po_functional_inspection_date': fields.related('purchase_line_id', 'order_id', 'fct_inspection_date',
                                                        type='date', string='Functional Inspection Date',
                                                        readonly=True),

        'po_packing_inspection_date': fields.related('purchase_line_id', 'order_id', 'pac_inspection_date',
                                                     type='date', string='Packing Inspection Date',
                                                     readonly=True),

        'related_po_number': fields.related('purchase_line_id', 'order_id', 'name',
                                            type='char', string='Related PO No.', readonly=True),

        'po_amount': fields.related('purchase_line_id', 'order_id', 'amount_total', type='float',
                                    string='PO Value', readonly=True),

        'move_quantity': fields.related('move_id', 'product_uos_qty', type='float', string='Quantity in move'),

        'port_of_loading': fields.function(_get_port, type='many2one', relation='stock.port', string='Port of Loading',
                                           multi='_get_port'),
        'port_of_discharge': fields.function(_get_port, type='many2one', relation='stock.port',
                                             string='Port of Discharge',
                                             multi='_get_port'),

        'etd': fields.function(_get_port, type='date', string='ETD', multi='_get_port'),
        'eta': fields.function(_get_port, type='date', string='ETA', multi='_get_port'),
        'vessel_freight': fields.function(_get_port, type='char', string='Vessel/Freight', multi='_get_port'),

        'invoice_number': fields.function(_get_invoice, type='char', string='Invoice number', multi='_get_invoice'),
        'due_date': fields.function(_get_invoice, type='date', string='Due Date', multi='_get_invoice'),

        'state': fields.function(_state, type='selection', selection=[('open', 'Open'),
                                                                      ('done', 'Done')],
                                 string='Status', readonly=True, select=True, store={
                'zwip.zwip': (lambda self, cr, uid, ids, ctx: ids, [], 10),
                'stock.move': (_get_stock_move, ['state'], 10),
                # 'sale.order.line': (_get_zwip_ids, ['invoice_lines'], 10),
                'account.invoice.line': (_get_so_line_ids, ['quantity'], 10),
            }),

        'outstanding_qty': fields.function(_get_qty_in_picking, type='float', string='Outstanding Quantity',
                                           multi='_get_qty_in_picking', store={
                'stock.move': (_get_stock_move, ['state'], 10),
            })
    }

    def button_update_zwip(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('stock.move')
        lst_zwip1 = self.search(cr, uid, [])

        for zwip in self.browse(cr, uid, lst_zwip1):
            if zwip.move_id:
                self.write(cr, uid, zwip.id, {
                    'sale_line_id': zwip.move_id and zwip.move_id.sale_line_id and zwip.move_id.sale_line_id.id or False
                })

            res = self._get_qty_in_picking(cr, uid, [zwip.id], False, False, context=context)
            state = self._state(cr, uid, [zwip.id], False, False, context=context)
            cr.execute(
                "update zwip_zwip set outstanding_qty = %s, state = %s where id = %s",
                (res[zwip.id]['outstanding_qty'],
                 state[zwip.id], zwip.id))
        return True
