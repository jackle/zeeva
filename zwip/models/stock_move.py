from datetime import date
from openerp.tools.translate import _
from osv import osv, fields
from openerp import SUPERUSER_ID
import openerp.addons.decimal_precision as dp


class stock_move(osv.osv):
    _inherit = 'stock.move'

    _columns = {
        'product_qty': fields.float('Quantity', digits_compute=dp.get_precision('Product Unit of Measure'),
                                    required=True, states={'done': [('readonly', True)]},
                                    help="This is the quantity of products from an inventory "
                                         "point of view. For moves in the state 'done', this is the "
                                         "quantity of products that were actually moved. For other "
                                         "moves, this is the quantity of product that is planned to "
                                         "be moved. Lowering this quantity does not generate a "
                                         "backorder. Changing this quantity on assigned moves affects "
                                         "the product reservation, and should be done with care."
                                    ),
        'sale_line_id': fields.many2one('sale.order.line', 'Sales Order Line', ondelete='set null', select=True,
                                        domain=[('balance_line_qty', '>', 0)]),
        'product_qty_in_picking': fields.float('Quantity in Packing List',
                                               digits_compute=dp.get_precision('Product Unit of Measure'), ),
        'product_qty_balance_sol': fields.float('Quantity Balance',
                                                digits_compute=dp.get_precision('Product Unit of Measure'), ),
    }

    def onchange_sol_id(self, cr, uid, ids, sol_id):
        result = {}
        domain = {}
        sol_obj = self.pool.get('sale.order.line')
        if sol_id:
            lst_move = self.search(cr, uid, [('sale_line_id', '=', sol_id), ('state', 'in', ['done'])])

            sol = sol_obj.browse(cr, uid, sol_id)
            balance_qty = sol.product_uom_qty - sum(line.product_qty for line in self.browse(cr, uid, lst_move))
            if balance_qty > 0:
                result.update({'product_id': sol.product_id and sol.product_id.id or False,
                               'product_qty': balance_qty > 0 and balance_qty or 0,
                               'product_uom': sol.product_id and sol.product_id.uom_id and sol.product_id.uom_id.id or False,
                               'product_qty_balance_sol': balance_qty
                               })

        return {'value': result,}

    def onchange_product_id(self, cr, uid, ids, prod_id=False, loc_id=False,
                            loc_dest_id=False, partner_id=False):
        result = super(stock_move, self).onchange_product_id(cr, uid, ids, prod_id, loc_id, loc_dest_id, partner_id)
        return {'value': result}

    def create(self, cr, uid, vals, context=None):
        res = super(stock_move, self).create(cr, uid, vals, context)
        zwip_obj = self.pool.get('zwip.zwip')
        if vals.get('sale_line_id', False):
            sale_line_id = vals['sale_line_id']
            lst_zwip = zwip_obj.search(cr, uid, [('sale_line_id', '=', sale_line_id)])
            zwip_obj.write(cr, uid, lst_zwip, {
                'move_id': res
            })
        return res

    def onchange_quantity_zeeva(self, cr, uid, ids, product_id, product_qty,
                          product_uom, product_uos, product_qty_balance_sol):

        result = {
            'product_uos_qty': 0.00
        }

        warning = {}

        if (not product_id) or (product_qty <= 0.0):
            result['product_qty'] = 0.0
            return {'value': result}

        product_obj = self.pool.get('product.product')
        uos_coeff = product_obj.read(cr, uid, product_id, ['uos_coeff'])
        if product_qty > product_qty_balance_sol:
            raise osv.except_osv(_('Warning!'), _(
                "the quantity should be <= %s") % (product_qty_balance_sol,))

        # Warn if the quantity was decreased
        if ids:
            for move in self.read(cr, uid, ids, ['product_qty']):
                if product_qty < move['product_qty']:
                    warning.update({
                        'title': _('Information'),
                        'message': _("By changing this quantity here, you accept the "
                                     "new quantity as complete: OpenERP will not "
                                     "automatically generate a back order.")})
                break

        if product_uos and product_uom and (product_uom != product_uos):
            result['product_uos_qty'] = product_qty * uos_coeff['uos_coeff']
        else:
            result['product_uos_qty'] = product_qty

        return {'value': result, 'warning': warning}
