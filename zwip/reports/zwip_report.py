# -*- coding: utf-8 -*-
######################################################################
#
# OpenERP, Open Source Management Solution
# Copyright (C) 2011 OpenERP s.a. (<http://openerp.com>).
# Copyright (C) 2013 INIT Tech Co., Ltd (http://init.vn).
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
######################################################################


import xlwt, datetime
from xlwt import *
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

VALUE = [
    ('Order Type', 18, 'order_type'),
    ('Merchandiser', 18, 'merchandiser'),
    ('SC NO', 18, 'sc_no'),
    ('PO NO', 18, 'po_no'),
    ('Customer PO Ref. No.', 18, 'cus_ref'),
    ('Customer', 30, 'customer'),
    ('Supplier', 30, 'supplier'),
    ('SC Confirmation Date', 15, 'sc_date'),
    ('Delivery Date', 15, 'delivery_date'),
    ('Item Code', 15, 'item_code'),
    ('Quantity', 15, 'qty'),
    ('Outstanding Quantity', 15, 'out_qty'),
    # ('Quantity in Packing List', 15, 'product_qty_in_picking'),
    # ('Quantity Shipped', 15, 'qty_shiped'),
    # ('Quantity to be Packed', 15, 'qty_to_be_packed'),
    # ('SD CROSS CHECKS', 15, ''),
    # ('Column1', 15, ''),
]


class ZwipReportParser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        if context is None:
            context = {}
        super(ZwipReportParser, self).__init__(cr, uid, name, context=context)
        self.context = context
        self.localcontext.update({
            'get_data': self.get_data,
        })

    def set_context(self, objects, data, ids, report_type=None):
        super(ZwipReportParser, self).set_context(objects, data, ids)
        self.localcontext.update({
            'zwip_ids': data.get('zwip_ids', [])
        })

    def get_data(self, cr, uid, ids):
        zwip_obj = self.pool.get('zwip.zwip')
        res = []
        for line in zwip_obj.browse(cr, uid, ids):
            res.append({
                'order_type': line.order_type and dict(self.pool.get('zwip.zwip').fields_get(cr, uid, allfields=['order_type'])['order_type']['selection'])[line.order_type] or '',
                'merchandiser': line.merchandiser_id and line.merchandiser_id.name or '',
                'sc_no': line.sale_id and line.sale_id.name or '',
                'po_no': line.purchase_id and line.purchase_id.name or '',
                'cus_ref': line.cust_po_ref and line.cust_po_ref or '',
                'customer': line.partner_id and line.partner_id.name or '',
                'supplier': line.supplier and line.supplier.name or '',
                'sc_date': line.sc_date_confirmed and datetime.datetime.strptime(line.sc_date_confirmed,
                                                                                      DEFAULT_SERVER_DATE_FORMAT).strftime('%d-%b-%y') or '',
                'delivery_date': line.po_delivery_date and datetime.datetime.strptime(line.po_delivery_date,
                                                                                      DEFAULT_SERVER_DATE_FORMAT).strftime(
                    '%d-%b-%y') or '',
                'item_code': line.item_code or '',
                'qty': line.sc_qty or 0.0,
                'out_qty': line.outstanding_qty or 0.0,
            })
        return res

class ZwipReportXls(report_xls):
    def __init__(self, name, table, rml=False, parser=False, header=True, store=False):
        super(ZwipReportXls, self).__init__(name, table, rml, parser, header, store)
        # Cell Styles
        _xs = self.xls_styles
        _xs.update({
            'fill_grey': 'pattern: pattern solid, fore_color 22;',
            'fill_blue': 'pattern: pattern solid, fore_color 27;',
            'fill_xx': 'pattern: pattern solid, fore_color 23;',
            'fill_yy': 'pattern: pattern solid, fore_color 24;',
            'borders_all_black':
                'borders: left thin, right thin, top thin, bottom thin;',
            'borders_top_bottom_black':
                'borders: left thin, right thin, top thin, bottom thin, '
                'left_colour %s, right_colour %s, '
                'top_colour %s, bottom_colour %s;'
                % (self._bc, self._bc, 0, 0),
            'vert_middle': 'align: vert center;',
        })

        # normal data
        normal = _xs['center'] + _xs['vert_middle']
        self.normal = xlwt.easyxf(normal)

        # Header format
        header_cell_format = _xs['center'] + _xs['borders_all_black'] + _xs['bold'] + _xs['vert_middle'] + \
                             _xs['wrap'] + _xs['fill']
        self.header_cell_style = xlwt.easyxf(header_cell_format)

    def _print_report_header(self, ws, _p, row_pos, _xs):
        # update zwip_zwip set purchase_line_id = null where purchase_line_id not in (select id from purchase_order_line);
        style = XFStyle()
        style.font.name = 'Calibri'
        style.font.bold = True
        style.alignment.wrap = True
        style.alignment.horz = Alignment.HORZ_LEFT
        style.alignment.vert = Alignment.VERT_CENTER
        style.borders.left = Borders.THIN
        style.borders.right = Borders.THIN
        style.borders.top = Borders.THIN
        style.borders.bottom = Borders.THIN

        pt_blue = Pattern()
        pt_blue.pattern = Pattern.SOLID_PATTERN
        pt_blue.pattern_fore_colour = 0x31  # Blue Background

        pt_lblue = Pattern()
        pt_lblue.pattern = Pattern.SOLID_PATTERN
        pt_lblue.pattern_fore_colour = 0x30  # Light blue Background

        style.pattern = pt_blue

        size = [size for (head, size, key) in VALUE]
        c_size = [('{}'.format(i), 1, value, 'text', None) for i, value in enumerate(size)]
        row_data = self.xls_row_template(c_size, [x[0] for x in c_size])
        row_pos = self.xls_write_row(ws, row_pos, row_data, set_column_size=True)

        ws.row(row_pos).height_mismatch = True
        ws.row(row_pos).height = 20 * 35
        for index, value in enumerate(VALUE):
            ws.write(row_pos, index, value[0], style)
        row_pos += 1
        return row_pos

    def _print_data(self, ws, _p, row_pos, _xs):
        data = _p.get_data(self.cr, self.uid, _p.zwip_ids)
        for val in data:
            for index, value in enumerate(VALUE):
                ws.write(row_pos, index, val.get(value[2], ''))
            row_pos += 1
        return row_pos

    def generate_xls_report(self, _p, _xs, data, objects, wb):
        ws = wb.add_sheet('OOR')
        ws.panes_frozen = True
        ws.set_horz_split_pos(1)
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Report Header
        row_pos = self._print_report_header(ws, _p, row_pos, _xs)
        row_pos = self._print_data(ws, _p, row_pos, _xs)


ZwipReportXls(
    'report.zwip_report',
    'zwip.report.wizard',
    parser=ZwipReportParser)