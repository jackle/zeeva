# -*- coding:utf-8 -*-
#
#
#    Copyright (C) 2011,2013 Michael Telahun Makonnen <mmakonnen@gmail.com>.
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

{
    'name': 'Zeeva Module',
    'version': '1.0',
    'category': 'Generic Modules',
    'author': 'OSCG',
    'description': '',

    'depends': [
        'base', 'mail', 'product', 'sale', 'purchase',
        'stock', 'zwip', 'zeeva_customs'
    ],
    'update_xml': [
        'security/custom_security.xml',
        'views/res_partner_view.xml',
        'views/zwip_view.xml',
    ],
    'test': [
    ],
    'demo_xml': [
    ],
    'installable': True,
    'auto_install': False,
    'application': False,

}
