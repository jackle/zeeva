# -*- coding:utf-8 -*-
from osv import osv, fields


class zwip_report_wizard(osv.osv_memory):
    _name = 'zwip.report.wizard'

    _columns = {
        'po_delivery_date_from': fields.date('Delivery Date from'),
        'po_delivery_date_to': fields.date('Delivery Date to'),

    }

    def button_print(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        zwip_ids = self.pool.get('zwip.zwip').search(cr, uid, [('state', '=', 'open'), ('sale_line_id', '!=', False)], order='name desc', context=context)
        datas = {
            'zwip_ids': zwip_ids,
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'zwip_report',
            'datas': datas,
            'name': 'OOR',
        }
