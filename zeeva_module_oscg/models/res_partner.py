from openerp.tools.translate import _
from osv import osv, fields
from openerp import SUPERUSER_ID


class res_partner(osv.osv):
    _inherit = 'res.partner'

    _columns={
        'is_a_vendor': fields.boolean('Vendor'),
        'is_a_testing_lab': fields.boolean('Testing Lab'),
    }

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = []
        for record in self.browse(cr, uid, ids, context=context):
            name = record.name
            if record.parent_id and not record.is_company:
                if record.is_sub_company:
                   name =  "%s [%s]" % (name, record.subcompany_name)
                else:
                    name =  "%s [%s]" % (name, record.parent_id.name)
            if context.get('show_address'):
                name = name + "\n" + self._display_address(cr, uid, record, without_company=True, context=context)
                name = name.replace('\n\n','\n')
                name = name.replace('\n\n','\n')
            if context.get('show_email') and record.email:
                name = "%s <%s>" % (name, record.email)
            #an fix
            if not name:
                name = record.name
            res.append((record.id, name))
        return res

