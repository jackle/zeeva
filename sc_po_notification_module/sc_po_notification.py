#-*- coding:utf-8 -*-
#
#
#    Copyright (C) 2011,2013 Michael Telahun Makonnen <mmakonnen@gmail.com>.
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

from datetime import date
from openerp.tools.translate import _
from osv import osv, fields
from openerp import netsvc
from openerp import SUPERUSER_ID
import string

class sale_order(osv.osv):
    _name = 'sale.order'
    _inherit = 'sale.order'
    
    def create(self, cr, uid, vals, context=None):
        draft_quotation = vals.get('name')
        sc_po_notification_users = ['nitin', 'akshay', 'sweta.mayur.dhruva']
        sale_order_id = super(sale_order, self).create(cr, uid, vals, context=context)
        for x in self.browse(cr, uid, [sale_order_id]):
            draft_quotation_number = x.name
            customer = x.partner_id.name
            total_amount = x.amount_total
            sales_person = x.user_id.name
        subscribe_ids = []
        subscribe_ids += self.pool.get('res.users').search(cr, SUPERUSER_ID, [('login','in',sc_po_notification_users)])
        self.message_subscribe_users(cr, SUPERUSER_ID, [sale_order_id], user_ids=subscribe_ids, context=context)
        message1 = _("<b>The Draft SC %s has been created.</b><ul><li><b>Amount:</b> %s </li><li><b>Customer:</b> %s </li><li><b>Salesperson:</b> %s </li></ul>") % (draft_quotation_number,total_amount,customer,sales_person)
        self.message_post(cr, uid, [sale_order_id], body = message1, type='comment', subtype='mt_comment', context = context) 
        return sale_order_id
        
    #def write(self, cr, uid, ids, vals, context=None):    
        #print vals, "................................................................................................."
        #print vals.items()[0], "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
     #   write_id = super(sale_order, self).write(cr, uid, ids, vals, context=context)
     #   return write_id
        
    def unlink(self, cr, uid, ids, context=None):
        sale_orders = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []
        for s in sale_orders:
            if s['state'] in ['draft', 'cancel']:
                sc_po_notification_users = ['nitin', 'akshay', 'sweta.mayur.dhruva']
        
                for x in self.browse(cr, uid, ids, context=None):
                    form_id = x.id
                    draft_quotation_number = x.name
                    customer = x.partner_id.name
                    total_amount = x.amount_total
                    sales_person = x.user_id.name
                    sc_state = x.state
                if sc_state in ('draft','cancel'):
                    subscribe_ids = []
                    subscribe_ids += self.pool.get('res.users').search(cr, SUPERUSER_ID, [('login','in',sc_po_notification_users)])
                    self.message_subscribe_users(cr, SUPERUSER_ID, [form_id], user_ids=subscribe_ids, context=context)
                    message1 = _("<b>The SC %s has been deleted.</b><ul><li><b>Amount:</b> %s </li><li><b>Customer:</b> %s </li><li><b>Salesperson:</b> %s </li></ul>") % (draft_quotation_number,total_amount,customer,sales_person)
                    self.message_post(cr, uid, [form_id], body = message1, type='comment', subtype='mt_comment', context = context) 
                unlink_ids.append(s['id'])
            else:
                raise osv.except_osv(_('Invalid Action!'), _('In order to delete a confirmed sales order, you must cancel it before!'))

        return osv.osv.unlink(self, cr, uid, unlink_ids, context=context)    
            
    def action_button_confirm(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        wf_service = netsvc.LocalService('workflow')
        wf_service.trg_validate(uid, 'sale.order', ids[0], 'order_confirm', cr)

        for m in self.browse(cr, uid, ids, context=context):
            sale_order_number = m.name
            customer = m.partner_id.name
            sale_order_total_amount = m.amount_total
            sale_order_salesperson = m.user_id.name
            subscribe_ids = []
            zeeva_ind_management = ['nitin', 'akshay', 'sweta.mayur.dhruva']
            subscribe_ids += self.pool.get('res.users').search(cr, SUPERUSER_ID, [('login','in',zeeva_ind_management)])
            self.message_subscribe_users(cr, SUPERUSER_ID, [m.id], user_ids=subscribe_ids, context=context)
            #message1 = _("<b><p>Status: Draft SC --> Confirmed SC</p><p>The Draft SC  </p></b>")
            message1 = _("<b>The Sale Order %s has been confirmed.</b><ul><li><b>Amount:</b> %s </li><li><b>Customer:</b> %s </li><li><b>Salesperson:</b> %s </li></ul>") % (sale_order_number,sale_order_total_amount,customer,sale_order_salesperson)
            self.message_post(cr, uid, ids, body = message1, type='comment', subtype='mt_comment', context = context) 
        #force confirm the reset order #TODO: improve 
        for id in ids:
            for line in self.browse(cr, uid, id, context).order_line:
                line.write({'state': 'confirmed'})
        return self.write(cr, uid, ids, {'state': 'manual'})

               

        # redisplay the record as a sales order
        view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'zeeva_customs', 'zeeva_sales_order_form_view')
        view_id = view_ref and view_ref[1] or False,
        return {
            'type': 'ir.actions.act_window',
            'name': _('Sales Order'),
            'res_model': 'sale.order',
            'res_id': ids[0],
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'current',
            'nodestroy': True,
        }
    
    
    
    def action_cancel(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        if context is None:
            context = {}
        sale_order_line_obj = self.pool.get('sale.order.line')
        proc_obj = self.pool.get('procurement.order')
        for sale in self.browse(cr, uid, ids, context=context):
            for pick in sale.picking_ids:
                if pick.state not in ('draft', 'cancel'):
                    raise osv.except_osv(
                        _('Cannot cancel sales order!'),
                        _('You must first cancel all delivery order(s) attached to this sales order.'))
                if pick.state == 'cancel':
                    for mov in pick.move_lines:
                        proc_ids = proc_obj.search(cr, uid, [('move_id', '=', mov.id)])
                        if proc_ids:
                            for proc in proc_ids:
                                wf_service.trg_validate(uid, 'procurement.order', proc, 'button_check', cr)
            for r in self.read(cr, uid, ids, ['picking_ids']):
                for pick in r['picking_ids']:
                    wf_service.trg_validate(uid, 'stock.picking', pick, 'button_cancel', cr)
        
        sc_po_notification_users = ['nitin', 'akshay', 'sweta.mayur.dhruva']
        #sale_order_id = super(sale_order, self).create(cr, uid, vals, context=context)
        for x in self.browse(cr, uid, ids, context=None):
            form_id = x.id
            draft_quotation_number = x.name
            current_state = x.state
            customer = x.partner_id.name
            total_amount = x.amount_total
            sales_person = x.user_id.name
            if current_state == 'manual':
                current_state_name = 'Sale to Invoice'
            elif current_state == 'sent':
                current_state_name = 'Quotation Sent'
            elif current_state == 'draft':
                current_state_name = 'Draft Quotation'
            elif current_state == 'waiting_date':
                current_state_name = 'Waiting Schedule'
            elif current_state == 'progress':
                current_state_name = 'Sales Order'
            elif current_state == 'shipping_except':
                current_state_name = 'Shipping Exception'
            elif current_state == 'invoice_except':
                current_state_name = 'Invoice Exception'
            elif current_state == 'done':
                current_state_name = 'Done'                                                    
        subscribe_ids = []
        subscribe_ids += self.pool.get('res.users').search(cr, SUPERUSER_ID, [('login','in',sc_po_notification_users)])
        self.message_subscribe_users(cr, SUPERUSER_ID, [form_id], user_ids=subscribe_ids, context=context)
        message1 = _("<b>The Sale Order %s has been cancelled.</b><ul><li><b>Amount:</b> %s </li><li><b>Customer:</b> %s </li><li><b>Salesperson:</b> %s </li></ul><p><b>State Transition:</b> %s --> Cancelled</p>") % (draft_quotation_number,total_amount,customer,sales_person,current_state_name)
        self.message_post(cr, uid, [form_id], body = message1, type='comment', subtype='mt_comment', context = context) 
        
        return super(sale_order, self).action_cancel(cr, uid, ids, context=context)
      
sale_order()

class purchase_order(osv.osv):
    _name = 'purchase.order'
    _inherit = 'purchase.order'
    
    def create(self, cr, uid, vals, context=None):
        #draft_quotation = vals.get('name')
        sc_po_notification_users = ['nitin', 'akshay', 'sweta.mayur.dhruva']
        purchase_order_id = super(purchase_order, self).create(cr, uid, vals, context=context)
        for x in self.browse(cr, uid, [purchase_order_id]):
            draft_quotation_number = x.name
            supplier = x.partner_id.name
            total_amount = x.amount_total
            #sales_person = x.user_id.name
        subscribe_ids = []
        subscribe_ids += self.pool.get('res.users').search(cr, SUPERUSER_ID, [('login','in',sc_po_notification_users)])
        self.message_subscribe_users(cr, SUPERUSER_ID, [purchase_order_id], user_ids=subscribe_ids, context=context)
        message1 = _("<b>The Draft PO %s has been created.</b><ul><li><b>Amount:</b> %s </li><li><b>Supplier:</b> %s </li></ul>") % (draft_quotation_number,total_amount,supplier)
        self.message_post(cr, uid, [purchase_order_id], body = message1, type='comment', subtype='mt_comment', context = context) 
        return purchase_order_id
        
    
        
    def unlink(self, cr, uid, ids, context=None):
        purchase_orders = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []
        for s in purchase_orders:
            if s['state'] in ['draft','cancel']:
                sc_po_notification_users = ['nitin', 'akshay', 'sweta.mayur.dhruva']
        
                for x in self.browse(cr, uid, ids, context=None):
                    form_id = x.id
                    draft_quotation_number = x.name
                    customer = x.partner_id.name
                    total_amount = x.amount_total
                    #sales_person = x.user_id.name
                    po_state = x.state
                if po_state in ('draft','cancel'):
                    subscribe_ids = []
                    subscribe_ids += self.pool.get('res.users').search(cr, SUPERUSER_ID, [('login','in',sc_po_notification_users)])
                    self.message_subscribe_users(cr, SUPERUSER_ID, [form_id], user_ids=subscribe_ids, context=context)
                    message1 = _("<b>The PO %s has been deleted.</b><ul><li><b>Amount:</b> %s </li><li><b>Customer:</b> %s </li></ul>") % (draft_quotation_number,total_amount,customer)
                    self.message_post(cr, uid, [form_id], body = message1, type='comment', subtype='mt_comment', context = context) 
                unlink_ids.append(s['id'])
            else:
                raise osv.except_osv(_('Invalid Action!'), _('In order to delete a purchase order, you must cancel it first.'))

        # automatically sending subflow.delete upon deletion
        wf_service = netsvc.LocalService("workflow")
        for id in unlink_ids:
            wf_service.trg_validate(uid, 'purchase.order', id, 'purchase_cancel', cr)

        return super(purchase_order, self).unlink(cr, uid, unlink_ids, context=context)            
    
    def wkf_confirm_order(self, cr, uid, ids, context=None):
        
        todo = []
        for po in self.browse(cr, uid, ids, context=context):
            
            if not po.order_line:
                raise osv.except_osv(_('Error!'),_('You cannot confirm a purchase order without any purchase order line.'))
            for line in po.order_line:
                if line.state=='draft':
                    todo.append(line.id)
            #print "TEST", po.origin
            
            #Assign the new PO name using the SC name
            if po.origin <> False:
                
                #test existing valid PO name
                if po.name[0:2] <> 'PO':
                    new_name = 'PO' + po.origin[2:8] + '-'
                    letters = []
                    letters = list(string.ascii_uppercase)
                    
                    res = self.search(cr, uid, [('name','ilike',new_name)], context=context)
                    #print new_name, "RES", res, len(res)
                    
                    new_name = new_name + letters[len(res)]
                    
                    self.write(cr, uid, [po.id], {'name': new_name})
                
                else:
                    continue
                    
                
            else: #no SC ref, we use the sequence number
                new_name = 'PO' + po.name
                
                self.write(cr, uid, [po.id], {'name': new_name})
                

        self.pool.get('purchase.order.line').action_confirm(cr, uid, todo, context)
        
        for m in self.browse(cr, uid, ids, context=context):
            purchase_order_number = m.name
            supplier = m.partner_id.name
            purchase_order_total_amount = m.amount_total
            #sale_order_salesperson = m.user_id.name
            subscribe_ids = []
            zeeva_ind_management = ['nitin', 'akshay', 'sweta.mayur.dhruva']
            subscribe_ids += self.pool.get('res.users').search(cr, SUPERUSER_ID, [('login','in',zeeva_ind_management)])
            self.message_subscribe_users(cr, SUPERUSER_ID, [m.id], user_ids=subscribe_ids, context=context)
            #message1 = _("<b><p>Status: Draft SC --> Confirmed SC</p><p>The Draft SC  </p></b>")
            message1 = _("<b>The Purchase Order %s has been confirmed.</b><ul><li><b>Amount:</b> %s </li><li><b>Supplier:</b> %s </li></ul>") % (purchase_order_number,purchase_order_total_amount,supplier)
            self.message_post(cr, uid, ids, body = message1, type='comment', subtype='mt_comment', context = context) 
        
        for id in ids:
            self.write(cr, uid, [id], {'state' : 'confirmed', 'validator' : uid})
        return True  
    
    def action_cancel(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        for purchase in self.browse(cr, uid, ids, context=context):
            for pick in purchase.picking_ids:
                if pick.state not in ('draft','cancel'):
                    raise osv.except_osv(
                        _('Unable to cancel this purchase order.'),
                        _('First cancel all receptions related to this purchase order.'))
            for pick in purchase.picking_ids:
                wf_service.trg_validate(uid, 'stock.picking', pick.id, 'button_cancel', cr)
            for inv in purchase.invoice_ids:
                if inv and inv.state not in ('cancel','draft'):
                    raise osv.except_osv(
                        _('Unable to cancel this purchase order.'),
                        _('You must first cancel all receptions related to this purchase order.'))
                if inv:
                    wf_service.trg_validate(uid, 'account.invoice', inv.id, 'invoice_cancel', cr)
        
        
        sc_po_notification_users = ['nitin', 'akshay', 'sweta.mayur.dhruva']
        #sale_order_id = super(sale_order, self).create(cr, uid, vals, context=context)
        for x in self.browse(cr, uid, ids, context=None):
            form_id = x.id
            draft_quotation_number = x.name
            current_state = x.state
            supplier = x.partner_id.name
            total_amount = x.amount_total
            current_state_name = []
            #sales_person = x.user_id.name
            if current_state == 'draft':
                current_state_name = 'Draft PO'
            elif current_state == 'sent':
                current_state_name = 'RFQ Sent'
            elif current_state == 'confirmed':
                current_state_name = 'Waiting Approval'
            elif current_state == 'approved':
                current_state_name = 'Purchase Order'
            elif current_state == 'except_picking':
                current_state_name = 'Shipping Exception'
            elif current_state == 'except_invoice':
                current_state_name = 'Invoice Exception'
            elif current_state == 'done':
                current_state_name = 'Done'
            elif current_state == 'cancel':
                current_state_name = 'Cancelled'                                                    
        subscribe_ids = []
        subscribe_ids += self.pool.get('res.users').search(cr, SUPERUSER_ID, [('login','in',sc_po_notification_users)])
        self.message_subscribe_users(cr, SUPERUSER_ID, [form_id], user_ids=subscribe_ids, context=context)
        message1 = _("<b>The Purchase Order %s has been cancelled.</b><ul><li><b>Amount:</b> %s </li><li><b>Supplier:</b> %s </li></ul><p><b>State Transition:</b> %s --> Cancelled</p>") % (draft_quotation_number,total_amount,supplier,current_state_name)
        self.message_post(cr, uid, [form_id], body = message1, type='comment', subtype='mt_comment', context = context) 
        
        self.write(cr,uid,ids,{'state':'cancel'})

        for (id, name) in self.name_get(cr, uid, ids):
            wf_service.trg_validate(uid, 'purchase.order', id, 'purchase_cancel', cr)
            
        return True
      
purchase_order()

            
            
            
            
            
