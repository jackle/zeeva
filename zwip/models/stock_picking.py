from datetime import date
from openerp.tools.translate import _
from osv import osv, fields
from openerp import SUPERUSER_ID


class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    def action_recompute_fields(self, cr, uid, ids, context=None):
        option1 = 'Financial Year(2013-2014)'
        option2 = 'Financial Year(2014-2015)'
        option3 = 'Financial Year(2015-2016)'
        option4 = 'Financial Year(2016-2017)'
        option5 = 'Financial Year(2017-2018)'
        lst_picking_out = self.search(cr, uid, [('ship_date', '!=', False),
                                                ('type', '=', 'out')
                                                ])
        for pick in self.browse(cr, uid, lst_picking_out):
            if pick.ship_date:
                etd_date = pick.ship_date
                if etd_date >= '2013-04-01' and etd_date <= '2014-03-31':
                    option = option1
                if etd_date >= '2014-04-01' and etd_date <= '2015-03-31':
                    option = option2
                if etd_date >= '2015-04-01' and etd_date <= '2016-03-31':
                    option = option3
                if etd_date >= '2016-04-01' and etd_date <= '2017-03-31':
                    option = option4
                if etd_date >= '2017-04-01' and etd_date <= '2018-03-31':
                    option = option5
                self.write(cr, uid, pick.id, {'financial_year': option}, context=context)
        return True


class stock_picking_out(osv.osv):
    _inherit = 'stock.picking.out'

    def action_recompute_fields(self, cr, uid, ids, context=None):
        picking_obj = self.pool.get('stock.picking')
        return picking_obj.action_recompute_fields(cr, uid, ids, context=context)
