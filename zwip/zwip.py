from datetime import date
from openerp.tools.translate import _
from osv import osv, fields
from openerp import SUPERUSER_ID


class sale_order_line(osv.osv):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'

    def _get_customer_reference(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.order_id.client_order_ref:
                client_reference_no = x.order_id.client_order_ref

                #   print client_reference_no, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = client_reference_no

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_payment_term(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.order_id.payment_term:
                sc_payment_term = x.order_id.payment_term.id

                #   print sc_payment_term, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = sc_payment_term

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_sc_total_value(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.order_id.amount_total:
                sc_total_value = x.order_id.amount_total

                #   print sc_total_value, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = sc_total_value

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_sc_account_manager(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.order_id.user_id:
                sc_account_manager = x.order_id.user_id.id

                #   print sc_account_manager, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = sc_account_manager

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_order_type(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print x.order_id.order_type, "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.order_id.order_type:
                sc_order_type = x.order_id.order_type

                #   print sc_order_type, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = sc_order_type

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_mrd(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print x.order_id.ready_date, "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.order_id.ready_date:
                sc_mrd = x.order_id.ready_date

                #   print sc_mrd, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = sc_mrd

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_packing_list(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            form_id = x.id

            # search_packing_list = self.pool.get('stock.picking.out').search(cr,uid,[('name','=','Sick Leave (SL)')], context=context)

            if x.order_id.ready_date:
                sc_mrd = x.order_id.ready_date

                #   print sc_mrd, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = sc_mrd

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    '''def _get_purchase_order_record(self, cr, uid, ids, field_name, arg, context=None):
	  print ids, "-----------------------------------------------------------------------------------"
	  res= {}

	  purchase_order_search = self.pool.get('purchase.order').search(cr, uid, [('id', '<=', 3119)], context=context)
	  print purchase_order_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

	  #eta_date = ''
	  for record in self.pool.get('purchase.order').browse(cr, uid, purchase_order_search):
	    print "................................................................................................"
	    purchase_order_id = record.id
	    source_document = record.origin
	    print purchase_order_id, "nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"
	    print source_document, "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"
	    if source_document:

              if 'SC' in source_document:

                print "ddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"
                #variable = source_document.split(':')[0]
                variable1 = source_document.split(' ')[0]

                print variable1, "55555555555555555555555555555555555555555555555555555"

                if variable1:
                    subject = variable1
                    print subject, "1111111111111111111111111111111111111111111111111111111111"
                else:
                    subject = source_document
                    print subject, "22222222222222222222222222222222222222222222222222222222222"

                search_sale_order = self.pool.get('sale.order').search(cr,uid,[('name','=',subject)], context=context)


                if search_sale_order:
                    print search_sale_order, "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"
                    for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                        sale_order_name = y.name
                        print sale_order_name, "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk"
                        print purchase_order_id, sale_order_name, "nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"
                #if eta_date:
                #    res[x.id] = eta_date
                #else:
                #    res[x.id] = ''

              #else:
                        for x in self.browse(cr, uid, ids, context=context):
	                        sale_order = x.order_id.name
	                        print sale_order, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                                if sale_order == sale_order_name:

                                    res[x.id] = purchase_order_id

          return res '''

    # _order = 'id'
    _columns = {

        'customer_reference_no': fields.function(_get_customer_reference, type='char', string='Cust PO Ref#'),
        'payment_term': fields.function(_get_payment_term, type='many2one', obj="account.payment.term",
                                        string='SC Payment Term'),
        'sc_total_value': fields.function(_get_sc_total_value, type='float', string='SC Value (USD)'),
        'sc_account_manager': fields.function(_get_sc_account_manager, type='many2one', obj="res.users",
                                              string='Account Manager'),
        'order_type': fields.function(_get_order_type, type='selection',
                                      selection=[('new', 'New'), ('repeat', 'Repeat')], string="Order Type"),
        'merchandise_ready_date': fields.function(_get_mrd, type='date', string="SC Delivery Date"),
        'packing_list_no': fields.function(_get_packing_list, type='char', string='Packing List No.'),
        # 'purchase_order_record': fields.function(_get_purchase_order_record, type='many2one', obj="purchase.order", string='Purchase Order'),
    }


sale_order_line()


class stock_move(osv.osv):
    _name = 'stock.move'
    _inherit = 'stock.move'

    def _get_port_of_loading(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.picking_id.port_of_loading:
                port_of_loading = x.picking_id.port_of_loading.id

                res[x.id] = port_of_loading

            else:

                res[x.id] = None

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_port_of_discharge(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.picking_id.port_of_discharge:
                port_of_discharge = x.picking_id.port_of_discharge.id

                #   print sc_account_manager, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = port_of_discharge

            else:

                res[x.id] = None

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_etd(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.picking_id.ship_date:
                etd_date = x.picking_id.ship_date

                #   print sc_account_manager, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = etd_date

            else:

                res[x.id] = False

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_eta(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.picking_id.eta:
                eta_date = x.picking_id.eta

                #   print sc_account_manager, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = eta_date

            else:

                res[x.id] = False

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_vessel_freight(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.picking_id.vessel_name:
                vessel_freight = x.picking_id.vessel_name

                # abcd
                res[x.id] = vessel_freight

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_sale_order_number(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        sale_order_name = ''
        # stock_move_search = self.pool.get('stock.move').search(cr, uid, [('id', '=', 9)], context=context)
        # print stock_move_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

        # eta_date = ''
        # for x in self.pool.get('stock.move').browse(cr, uid, stock_move_search):
        for x in self.browse(cr, uid, ids, context=context):
            source_document = x.origin
            form_id = x.id


            if source_document:
                if 'SC' in source_document:

                    # variable = source_document.split(':')[0]
                    variable = source_document.split(' ')[0]


                    if variable:
                        subject = variable
                        # elif variable1:
                        #   subject = variable1
                        #  print subject, "44444444444444444444444444444444444444444444444444444444444"
                    else:
                        subject = source_document

                    search_sale_order = self.pool.get('sale.order').search(cr, uid, [('name', '=', subject)],
                                                                           context=context)

                    if search_sale_order:

                        for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                            sale_order_name = y.name

                    if sale_order_name:
                        res[x.id] = sale_order_name
                    else:
                        res[x.id] = ''

                else:

                    res[x.id] = ''

            else:

                res[x.id] = ''

        return res

    def _get_company(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        company = ''
        # stock_move_search = self.pool.get('stock.move').search(cr, uid, [('id', '=', 9)], context=context)
        # print stock_move_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

        # eta_date = ''
        # for x in self.pool.get('stock.move').browse(cr, uid, stock_move_search):
        for x in self.browse(cr, uid, ids, context=context):
            source_document = x.origin
            form_id = x.id


            if source_document:
                if 'SC' in source_document:

                    # variable = source_document.split(':')[0]
                    variable = source_document.split(' ')[0]


                    if variable:
                        subject = variable
                        # elif variable1:
                        #   subject = variable1
                        #  print subject, "44444444444444444444444444444444444444444444444444444444444"
                    else:
                        subject = source_document

                    search_sale_order = self.pool.get('sale.order').search(cr, uid, [('name', '=', subject)],
                                                                           context=context)

                    if search_sale_order:

                        for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                            company = y.company_id.id

                    if company:
                        res[x.id] = company
                    else:
                        res[x.id] = None

                else:

                    res[x.id] = None

            else:

                res[x.id] = None

        return res

    def _get_customer_reference(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        cust_reference = ''
        # stock_move_search = self.pool.get('stock.move').search(cr, uid, [('id', '=', 9)], context=context)
        # print stock_move_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

        # eta_date = ''
        # for x in self.pool.get('stock.move').browse(cr, uid, stock_move_search):
        for x in self.browse(cr, uid, ids, context=context):
            source_document = x.origin
            form_id = x.id


            if source_document:
                if 'SC' in source_document:

                    # variable = source_document.split(':')[0]
                    variable = source_document.split(' ')[0]


                    if variable:
                        subject = variable
                        # elif variable1:
                        #   subject = variable1
                        #  print subject, "44444444444444444444444444444444444444444444444444444444444"
                    else:
                        subject = source_document

                    search_sale_order = self.pool.get('sale.order').search(cr, uid, [('name', '=', subject)],
                                                                           context=context)

                    if search_sale_order:

                        for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                            cust_reference = y.client_order_ref

                    if cust_reference:
                        res[x.id] = cust_reference
                    else:
                        res[x.id] = ''

                else:

                    res[x.id] = ''

            else:

                res[x.id] = ''

        return res

    def _get_customer_name(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        cust_name = ''
        # stock_move_search = self.pool.get('stock.move').search(cr, uid, [('id', '=', 9)], context=context)
        # print stock_move_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

        # eta_date = ''
        # for x in self.pool.get('stock.move').browse(cr, uid, stock_move_search):
        for x in self.browse(cr, uid, ids, context=context):
            source_document = x.origin
            form_id = x.id


            if source_document:
                if 'SC' in source_document:

                    # variable = source_document.split(':')[0]
                    variable = source_document.split(' ')[0]


                    if variable:
                        subject = variable
                        # elif variable1:
                        #   subject = variable1
                        #  print subject, "44444444444444444444444444444444444444444444444444444444444"
                    else:
                        subject = source_document

                    search_sale_order = self.pool.get('sale.order').search(cr, uid, [('name', '=', subject)],
                                                                           context=context)

                    if search_sale_order:

                        for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                            cust_name = y.partner_id.id

                    if cust_name:
                        res[x.id] = cust_name
                    else:
                        res[x.id] = None

                else:

                    res[x.id] = None

            else:

                res[x.id] = None

        return res

    def _get_payment_term(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        payment_term = ''
        # stock_move_search = self.pool.get('stock.move').search(cr, uid, [('id', '=', 9)], context=context)
        # print stock_move_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

        # eta_date = ''
        # for x in self.pool.get('stock.move').browse(cr, uid, stock_move_search):
        for x in self.browse(cr, uid, ids, context=context):
            source_document = x.origin
            form_id = x.id


            if source_document:
                if 'SC' in source_document:

                    # variable = source_document.split(':')[0]
                    variable = source_document.split(' ')[0]


                    if variable:
                        subject = variable
                        # elif variable1:
                        #   subject = variable1
                        #  print subject, "44444444444444444444444444444444444444444444444444444444444"
                    else:
                        subject = source_document

                    search_sale_order = self.pool.get('sale.order').search(cr, uid, [('name', '=', subject)],
                                                                           context=context)

                    if search_sale_order:

                        for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                            payment_term = y.payment_term.id

                    if payment_term:
                        res[x.id] = payment_term
                    else:
                        res[x.id] = None

                else:

                    res[x.id] = None

            else:

                res[x.id] = None

        return res

    def _get_sc_total_value(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        sc_total_value = ''
        # stock_move_search = self.pool.get('stock.move').search(cr, uid, [('id', '=', 9)], context=context)
        # print stock_move_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

        # eta_date = ''
        # for x in self.pool.get('stock.move').browse(cr, uid, stock_move_search):
        for x in self.browse(cr, uid, ids, context=context):
            source_document = x.origin
            form_id = x.id


            if source_document:
                if 'SC' in source_document:

                    # variable = source_document.split(':')[0]
                    variable = source_document.split(' ')[0]


                    if variable:
                        subject = variable
                        # elif variable1:
                        #   subject = variable1
                        #  print subject, "44444444444444444444444444444444444444444444444444444444444"
                    else:
                        subject = source_document

                    search_sale_order = self.pool.get('sale.order').search(cr, uid, [('name', '=', subject)],
                                                                           context=context)

                    if search_sale_order:

                        for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                            sc_total_value = y.amount_total

                    if sc_total_value:
                        res[x.id] = sc_total_value
                    else:
                        res[x.id] = ''

                else:

                    res[x.id] = ''

            else:

                res[x.id] = ''

        return res

    def _get_sc_account_manager(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        sc_account_manager = ''
        # stock_move_search = self.pool.get('stock.move').search(cr, uid, [('id', '=', 9)], context=context)
        # print stock_move_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

        # eta_date = ''
        # for x in self.pool.get('stock.move').browse(cr, uid, stock_move_search):
        for x in self.browse(cr, uid, ids, context=context):
            source_document = x.origin
            form_id = x.id


            if source_document:
                if 'SC' in source_document:

                    # variable = source_document.split(':')[0]
                    variable = source_document.split(' ')[0]


                    if variable:
                        subject = variable
                        # elif variable1:
                        #   subject = variable1
                        #  print subject, "44444444444444444444444444444444444444444444444444444444444"
                    else:
                        subject = source_document

                    search_sale_order = self.pool.get('sale.order').search(cr, uid, [('name', '=', subject)],
                                                                           context=context)

                    if search_sale_order:

                        for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                            sc_account_manager = y.user_id.id

                    if sc_account_manager:
                        res[x.id] = sc_account_manager
                    else:
                        res[x.id] = None

                else:

                    res[x.id] = None

            else:

                res[x.id] = None

        return res

    def _get_order_type(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        sc_order_type = ''
        # stock_move_search = self.pool.get('stock.move').search(cr, uid, [('id', '=', 9)], context=context)
        # print stock_move_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

        # eta_date = ''
        # for x in self.pool.get('stock.move').browse(cr, uid, stock_move_search):
        for x in self.browse(cr, uid, ids, context=context):
            source_document = x.origin
            form_id = x.id


            if source_document:
                if 'SC' in source_document:

                    # variable = source_document.split(':')[0]
                    variable = source_document.split(' ')[0]


                    if variable:
                        subject = variable
                        # elif variable1:
                        #   subject = variable1
                        #  print subject, "44444444444444444444444444444444444444444444444444444444444"
                    else:
                        subject = source_document

                    search_sale_order = self.pool.get('sale.order').search(cr, uid, [('name', '=', subject)],
                                                                           context=context)

                    if search_sale_order:

                        for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                            sc_order_type = y.order_type

                    if sc_order_type:
                        res[x.id] = sc_order_type
                    else:
                        res[x.id] = ''

                else:

                    res[x.id] = ''

            else:

                res[x.id] = ''

        return res

    def _get_mrd(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        sc_mrd = ''
        # stock_move_search = self.pool.get('stock.move').search(cr, uid, [('id', '=', 9)], context=context)
        # print stock_move_search, "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

        # eta_date = ''
        # for x in self.pool.get('stock.move').browse(cr, uid, stock_move_search):
        for x in self.browse(cr, uid, ids, context=context):
            source_document = x.origin
            form_id = x.id


            if source_document:
                if 'SC' in source_document:

                    # variable = source_document.split(':')[0]
                    variable = source_document.split(' ')[0]


                    if variable:
                        subject = variable
                        # elif variable1:
                        #   subject = variable1
                        #  print subject, "44444444444444444444444444444444444444444444444444444444444"
                    else:
                        subject = source_document

                    search_sale_order = self.pool.get('sale.order').search(cr, uid, [('name', '=', subject)],
                                                                           context=context)

                    if search_sale_order:

                        for y in self.pool.get('sale.order').browse(cr, uid, search_sale_order):
                            sc_mrd = y.ready_date

                    if sc_mrd:
                        res[x.id] = sc_mrd
                    else:
                        res[x.id] = False

                else:

                    res[x.id] = False

            else:

                res[x.id] = False

        return res

    def _get_invoice_number(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.picking_id.invoice_id:
                invoice_number = x.picking_id.invoice_id.number

                #   print sc_account_manager, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = invoice_number

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_due_date(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.picking_id.invoice_id:
                due_date = x.picking_id.invoice_id.date_due

                #   print sc_account_manager, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = due_date

            else:

                res[x.id] = False

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    def _get_invoice_status(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for x in self.browse(cr, uid, ids, context=context):
            # print "ggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
            # product_id = x.product_id.id
            if x.picking_id.invoice_id:
                invoice_status = x.picking_id.invoice_id.state

                #   print sc_account_manager, "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
                res[x.id] = invoice_status

            else:

                res[x.id] = ''

                # self.write(cr, uid, x.id, {'product_category': product_category})
        return res

    _columns = {

        'port_of_loading': fields.function(_get_port_of_loading, type='many2one', obj="stock.port",
                                           string='Port of Loading', store=True),
        'port_of_discharge': fields.function(_get_port_of_discharge, type='many2one', obj="stock.port",
                                             string='Port of Discharge', store=True),
        'company_id': fields.function(_get_company, type='many2one', obj="res.company", string='Company', store=True),
        'customer_name': fields.function(_get_customer_name, type='many2one', obj="res.partner", string='Customer Name',
                                         store=True),
        'customer_reference_no': fields.function(_get_customer_reference, type='char', string='Cust PO Ref#',
                                                 store=True),
        'payment_term': fields.function(_get_payment_term, type='many2one', obj="account.payment.term",
                                        string='SC Payment Term', store=True),
        'sc_total_value': fields.function(_get_sc_total_value, type='float', string='SC Value (USD)', store=True),
        'sc_account_manager': fields.function(_get_sc_account_manager, type='many2one', obj="res.users",
                                              string='Account Manager', store=True),
        'etd_date': fields.function(_get_etd, type='date', string="ETD", store=True),
        'eta_date': fields.function(_get_eta, type='date', string="ETA", store=True),
        'vessel_freight': fields.function(_get_vessel_freight, type='char', string="Vessel/Freight"),
        'sale_order_number': fields.function(_get_sale_order_number, type='char', string="SC NO.", store=True),
        'order_type': fields.function(_get_order_type, type='selection',
                                      selection=[('new', 'New'), ('repeat', 'Repeat')], string="Order Type"),
        'merchandise_ready_date': fields.function(_get_mrd, type='date', string="SC Delivery Date"),
        'invoice_number': fields.function(_get_invoice_number, type='char', string="Invoice Number", store=True),
        'due_date': fields.function(_get_due_date, type='date', string="Due Date", store=True),
        'invoice_status': fields.function(_get_invoice_status, type='char', string="Status", store=True),
        # 'payment_term': fields.function(_get_payment_term, type='many2one', obj="account.payment.term", string='SC Payment Term'),
        # 'sc_total_value': fields.function(_get_sc_total_value, type='float', string='SC Value (USD)'),
        # 'sc_account_manager': fields.function(_get_sc_account_manager, type='many2one', obj="res.users", string='Account Manager'),
        # 'order_type': fields.function(_get_order_type, type='selection', selection=[('new','New'),('repeat','Repeat')], string="Order Type"),
        # 'merchandise_ready_date': fields.function(_get_mrd, type='date', string="SC Delivery Date"),
        # 'packing_list_no': fields.function(_get_packing_list, type='char', string='Packing List No.'),
        # 'purchase_order_record': fields.function(_get_purchase_order_record, type='many2one', obj="purchase.order", string='Purchase Order'),
    }


stock_move()


##----------------------------------------- CODE BEGINS HERE -----------------------------------------##

class zwip_class(osv.osv):
    _name = 'zwip.class'
    # _inherit = 'stock.move'

    _columns = {

        'sale_order_id': fields.integer('Sale Order ID'),
        'purchase_order_line_id': fields.integer('Purchase Order Line ID'),
        'stock_move_id': fields.integer('Stock Move ID'),
        'company_id': fields.many2one('res.company', 'Company'),
        # 'name': fields.char('Name'),
        'cust_po_ref': fields.char('Customer PO Ref. No.'),
        'sc_no': fields.char('SC No.'),
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'payment_term': fields.many2one('account.payment.term', 'SC Payment Term'),
        'sc_value': fields.float('SC Value (USD)'),
        'account_manager': fields.many2one('res.users', 'Account Manager'),
        'order_type': fields.selection([('new', 'New'), ('repeat', 'Repeat')], 'Order Type'),
        'sc_delivery_date': fields.date('SC Delivery Date'),
        'product_id': fields.many2one('product.product', 'Product'),
        'item_code': fields.char('Item Code'),
        'item_description': fields.char('Item Description'),
        'quantity': fields.integer('Quantity'),
        'supplier': fields.many2one('res.partner', 'Supplier'),
        'supplier_payment_term': fields.many2one('account.payment.term', 'Supplier Payment Term'),
        'po_delivery_date': fields.date('PO Delivery Date'),
        'po_functional_inspection_date': fields.date('Functional Inspection Date'),
        'po_packing_inspection_date': fields.date('Packing Inspection Date'),
        'related_po_number': fields.char('Related PO No.'),
        'port_of_loading': fields.many2one('stock.port', 'Port of Loading'),
        'port_of_discharge': fields.many2one('stock.port', 'Port of Discharge'),
        'etd': fields.date('ETD'),
        'eta': fields.date('ETA'),
        'vessel_freight': fields.char('Vessel/Freight'),
    }


zwip_class()


class sale_order(osv.osv):
    _name = 'sale.order'
    _inherit = 'sale.order'

    def create(self, cr, uid, vals, context=None):
        # company_id = vals.get('company_id')
        cust_po_ref = vals.get('client_order_ref')
        sc_no = vals.get('name')
        partner_id = vals.get('partner_id')
        payment_term = vals.get('payment_term')
        company_id = vals.get('shop_id')
        amount_total = vals.get('amount_total')
        account_manager = vals.get('user_id')
        order_type = vals.get('order_type')
        sc_delivery_date = vals.get('ready_date')
        # sale_order_id = vals.get('id')

        sale_order_id = super(sale_order, self).create(cr, uid, vals, context=context)

        if company_id == 1:
            company_id_value = self.pool.get('res.company').search(cr, uid,
                                                                   [('name', '=', 'Zeeva International Limited')])
        elif company_id == 2:
            company_id_value = self.pool.get('res.company').search(cr, uid, [('name', '=', 'S S Enterprises')])
        create_id1 = self.pool.get('zwip.class').create(cr, uid,
                                                        {'company_id': company_id_value[0], 'cust_po_ref': cust_po_ref,
                                                         'sc_no': sc_no, 'partner_id': partner_id,
                                                         'payment_term': payment_term, 'sc_value': amount_total,
                                                         'sale_order_id': sale_order_id,
                                                         'account_manager': account_manager, 'order_type': order_type,
                                                         'sc_delivery_date': sc_delivery_date}, context=context)
        return sale_order_id



        # return write_id


sale_order()


class purchase_order_line(osv.osv):
    _name = 'purchase.order.line'
    _inherit = 'purchase.order.line'

    def create(self, cr, uid, vals, context=None):
        # company_id = vals.get('company_id')
        product_id = vals.get('product_id')
        item_description = vals.get('name')
        quantity = vals.get('product_qty')
        purchase_order_id = vals.get('order_id')

        search_purchase_order = self.pool.get('purchase.order').search(cr, uid, [('id', '=', purchase_order_id)])

        for m in self.pool.get('purchase.order').browse(cr, uid, search_purchase_order):
            supplier = m.partner_id.id
            supplier_payment_term = m.payment_term_id.id
            po_delivery_date = m.ready_date
            po_functional_inspection_date = m.fct_inspection_date
            po_packing_inspection_date = m.pac_inspection_date
            po_number = "Draft PO:" + m.name

        purchase_order_line_id = super(purchase_order_line, self).create(cr, uid, vals, context=context)

        create_id1 = self.pool.get('zwip.class').create(cr, uid,
                                                        {'product_id': product_id, 'item_description': item_description,
                                                         'quantity': quantity, 'supplier': supplier,
                                                         'supplier_payment_term': supplier_payment_term,
                                                         'po_delivery_date': po_delivery_date,
                                                         'po_functional_inspection_date': po_functional_inspection_date,
                                                         'po_packing_inspection_date': po_packing_inspection_date,
                                                         'related_po_number': po_number,
                                                         'purchase_order_line_id': purchase_order_line_id},
                                                        context=context)
        return purchase_order_line_id


class purchase_order(osv.osv):
    _name = 'purchase.order'
    _inherit = 'purchase.order'

    def write(self, cr, uid, ids, vals, context=None):
        # if alias_model has been changed, update alias_model_id accordingly
        res = super(purchase_order, self).write(cr, uid, ids, vals)
        purchase_order_line_obj = self.pool.get('purchase.order.line')
        if isinstance(ids, int):
            line_main_id = ids
        else:
            line_main_id = ids[0]
        search_purchase_order_line_id = purchase_order_line_obj.search(cr, uid, [('order_id', '=', line_main_id)])
        if search_purchase_order_line_id:
            for o in purchase_order_line_obj.browse(cr, uid, search_purchase_order_line_id):
                main_id = o.id
                order_id = o.order_id.id
                purchase_order_line_product_id = o.product_id.id
                purchase_order_line_product_code = o.product_id.default_code
                purchase_order_line_item_description = o.name
                purchase_order_line_product_quantity = o.product_qty


            search_purchase_order_id = self.pool.get('purchase.order').search(cr, uid, [('id', '=', order_id)])

            for t in self.pool.get('purchase.order').browse(cr, uid, search_purchase_order_id):
                main_form_id = t.id
                supplier_id = t.partner_id.id
                supplier_payment_term = t.payment_term_id.id
                # amount_total = t.amount_total
                po_delivery_date = t.ready_date
                po_functional_inspection_date = t.fct_inspection_date
                po_pac_inspection_date = t.pac_inspection_date
                # item_description = t.name

            search_zwip_class = self.pool.get('zwip.class').search(cr, uid, [('purchase_order_line_id', '=', main_id)])

            for m in self.pool.get('zwip.class').browse(cr, uid, search_zwip_class):
                zwip_id = m.id
                self.pool.get('zwip.class').write(cr, uid, m.id, {'product_id': purchase_order_line_product_id,
                                                                  'item_description': purchase_order_line_item_description,
                                                                  'quantity': purchase_order_line_product_quantity,
                                                                  'supplier': supplier_id,
                                                                  'supplier_payment_term': supplier_payment_term,
                                                                  'po_delivery_date': po_delivery_date,
                                                                  'item_description': purchase_order_line_item_description,
                                                                  'po_functional_inspection_date': po_functional_inspection_date,
                                                                  'po_packing_inspection_date': po_pac_inspection_date,
                                                                  'item_code': purchase_order_line_product_code})

        return res


class stock_move(osv.osv):
    _name = 'stock.move'
    _inherit = 'stock.move'

    def create(self, cr, uid, vals, context=None):
        # company_id = vals.get('company_id')
        product_id = vals.get('product_id')
        item_description = vals.get('name')
        # quantity = vals.get('product_qty')
        picking_id = vals.get('picking_id')

        search_packing_list = self.pool.get('stock.picking').search(cr, uid, [('id', '=', picking_id)])

        port_of_loading = []
        port_of_discharge = []
        etd = ''
        eta = ''
        vessel_freight = []

        for m in self.pool.get('stock.picking').browse(cr, uid, search_packing_list):
            form_id = m.id
            port_of_loading = m.port_of_loading.id
            port_of_discharge = m.port_of_discharge.id
            etd = m.ship_date
            eta = m.eta
            vessel_freight = m.vessel_name

        stock_move_id = super(stock_move, self).create(cr, uid, vals, context=context)

        if etd == '' or eta == '':

            create_id1 = self.pool.get('zwip.class').create(cr, uid, {'product_id': product_id,
                                                                      'item_description': item_description,
                                                                      'port_of_loading': port_of_loading,
                                                                      'port_of_discharge': port_of_discharge,
                                                                      'vessel_freight': vessel_freight,
                                                                      'stock_move_id': stock_move_id}, context=context)

        else:

            create_id1 = self.pool.get('zwip.class').create(cr, uid, {'product_id': product_id,
                                                                      'item_description': item_description,
                                                                      'port_of_loading': port_of_loading,
                                                                      'port_of_discharge': port_of_discharge,
                                                                      'etd': etd, 'eta': eta,
                                                                      'vessel_freight': vessel_freight,
                                                                      'stock_move_id': stock_move_id}, context=context)

        return stock_move_id

    def write(self, cr, uid, ids, vals, context=None):
        write_id = super(stock_move, self).write(cr, uid, ids, vals, context=context)
        for x in self.browse(cr, uid, ids, context=None):
            stock_move_id = x.id
            stock_move_product_id = x.product_id.id
            stock_move_item_description = x.name

            # search_stock_picking = self.pool.get('stock.picking').search(cr, uid, [('id', '=', stock_picking_id)])
            # print search_stock_picking, "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"

            search_zwip_class = self.pool.get('zwip.class').search(cr, uid, [('stock_move_id', '=', stock_move_id)])

            for m in self.pool.get('zwip.class').browse(cr, uid, search_zwip_class):
                zwip_id = m.id
                self.pool.get('zwip.class').write(cr, uid, m.id, {'product_id': stock_move_product_id,
                                                                  'item_description': stock_move_item_description})

        return write_id


class stock_picking_out(osv.osv):
    _name = 'stock.picking.out'
    _inherit = 'stock.picking.out'

    def write(self, cr, uid, ids, vals, context=None):
        write_id = super(stock_picking_out, self).write(cr, uid, ids, vals, context=context)
        for m in self.browse(cr, uid, ids, context=None):
            form_id = m.id
            port_of_loading = m.port_of_loading.id
            port_of_discharge = m.port_of_discharge.id
            etd = m.ship_date
            eta = m.eta
            vessel_freight = m.vessel_name

        search_stock_move = self.pool.get('stock.move').search(cr, uid, [('picking_id', '=', form_id)])

        for j in self.pool.get('stock.move').browse(cr, uid, search_stock_move):
            move_id = j.id

            search_zwip_master = self.pool.get('zwip.class').search(cr, uid, [('stock_move_id', '=', move_id)])


            for m in self.pool.get('zwip.class').browse(cr, uid, search_zwip_master):
                zwip_id = m.id
                self.pool.get('zwip.class').write(cr, uid, m.id, {'port_of_loading': port_of_loading,
                                                                  'port_of_discharge': port_of_discharge, 'etd': etd,
                                                                  'eta': eta, 'vessel_freight': vessel_freight})

        return write_id
